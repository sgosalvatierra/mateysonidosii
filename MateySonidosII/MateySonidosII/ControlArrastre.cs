﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace MateySonidosII
{
    public class ControlArrastre : INotifyPropertyChanged
    {
        bool permitirArrastrar = false;
        public bool PermitirArrastrar
        {
            get => permitirArrastrar;
            set
            {
                if (permitirArrastrar == value)
                    return;
                permitirArrastrar = value;
                OnPropoertyChanged(nameof(PermitirArrastrar));
            }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropoertyChanged(string permitirArrastrar)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(permitirArrastrar));
        }

    }
}
