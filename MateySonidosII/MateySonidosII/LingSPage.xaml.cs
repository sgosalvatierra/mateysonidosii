﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Plugin.SimpleAudioPlayer;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MateySonidosII
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LingSPage : ContentPage
    {
        public event Action Completed;
        public event Action Ticked;
        public Boolean ticked;
        public Boolean _isrunning;
        public Boolean _ispausa;
        public TimeSpan Time;
        public TimeSpan TiempoTotal;
        public TimeSpan TiempoRestante;
        public int Duracion; // duracion del ejercicio en segundos
        public int[] TiemposEjecutar;
        public List<int> Resultados;
        public int IndiceTiemposEjecutar;
        public ISimpleAudioPlayer player;
        public Boolean _unicavez;
        public int aciertos;
        public int desaciertos;
        public int CantidadRepeticion = 3; //cantidad de veces que quiero se repetia el sonido por ejercicio

        //public int[] Segundos;
        public int DuracionSonido = 2;

        public LingSPage()
        {
            InitializeComponent();
            var stream = GetStreamFromFile("s1_8.mp3");
            player = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
            player.Load(stream);

            InitControls();
        }

        Stream GetStreamFromFile(string filename)
        {
            var assembly = typeof(App).GetTypeInfo().Assembly;

            var stream = assembly.GetManifestResourceStream("MateySonidosII." + filename);

            return stream;
        }

        private void InitControls()
        {
            btnPlay.Clicked += BtnPlayClicked;
            btnPausa.Clicked += BtnPausaClicked;
            btnStop.Clicked += BtnStopClicked;
            btnImagen.Pressed += btnImagenPressed;
            btnImagen.Released += btnImagenRealeased;
            _isrunning = true;
            _ispausa = false;
            Ticked += OnTicked;
            Completed += OnCompleted;

            player.PlaybackEnded += EndPlay;

            _unicavez = true;
            Resultados = new List<int>();
            aciertos = 0;
            desaciertos = 0;


            Duracion = 30;
            Time = TimeSpan.Zero;
            TiempoTotal = new TimeSpan(0, 0, Duracion);
            Tiemposlider.Maximum = Duracion;
            TiempoRestante = new TimeSpan();
            TiempoRestante = TimeSpan.Zero;
        }

        private void EndPlay(object sender, EventArgs e)
        {
            if (_unicavez)
            {
                Resultados.Add(0);
                desaciertos++;
            }
            else
            {
                _unicavez = true;
            }

        }

        private void btnImagenRealeased(object sender, EventArgs e)
        {
            FrameImagen.BackgroundColor = Color.Default;
            btnImagen.BackgroundColor = Color.Default;
        }

        private void btnImagenPressed(object sender, EventArgs e)
        {
            if (player.IsPlaying)
            {
                FrameImagen.BackgroundColor = Color.Green;
                btnImagen.BackgroundColor = Color.Green;
                if (_unicavez)
                {
                    _unicavez = false;
                    Resultados.Add(1);
                    aciertos++;
                }

            }
            else
            {
                FrameImagen.BackgroundColor = Color.Red;
                btnImagen.BackgroundColor = Color.Red;
                Resultados.Add(0);
                desaciertos++;
            }
        }
        private void BtnStopClicked(object sender, EventArgs e)
        {
            _isrunning = false;
            btnPlay.IsEnabled = true;
            Completed?.Invoke();

        }

        private void BtnPausaClicked(object sender, EventArgs e)
        {
            _isrunning = false;
            _ispausa = true;
            //btnPlay.IsEnabled = true;

        }

        private void BtnPlayClicked(object sender, EventArgs e)
        {
            //si vuelvo de una pausa no ejeuto
            if (!_ispausa)
            {
                TiemposEjecutar = GenerarTiemposEjecucion();
                Array.Sort(TiemposEjecutar);
                IndiceTiemposEjecutar = 0;
            }

            //btnPlay.IsEnabled = false;
            _isrunning = true;
            _ispausa = false;


            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                ticked = (Time.TotalSeconds < TiempoTotal.TotalSeconds) && _isrunning;
                if (ticked)
                {
                    Ticked?.Invoke();
                }
                else
                {
                    Completed?.Invoke();
                }
                return ticked;
            });

        }

        private int[] GenerarTiemposEjecucion()
        {
            var random = new Random();
            //cantidad de numero aleatorios que quiero
            var SegundosRandom = new int[CantidadRepeticion];

            // la primera vez
            int numaleatorio = random.Next(1, Duracion - DuracionSonido);
            SegundosRandom[0] = numaleatorio;

            for (int i = 1; i < CantidadRepeticion;)
            {
                numaleatorio = random.Next(1, Duracion - DuracionSonido);
                // veo si esta permitido
                if (permitido(numaleatorio, SegundosRandom))
                {
                    SegundosRandom[i] = numaleatorio;
                    i++;
                }
            }

            Array.Sort(SegundosRandom);
            //PruebaSegundos.Text = string.Join(", ", SegundosRandom);
            return SegundosRandom;


        }

        private bool permitido(int numaleatorio, int[] segundosRandom)
        {
            bool posible = true;
            for (int i = 0; i < CantidadRepeticion && posible; i++)
            {
                if ((numaleatorio >= segundosRandom[i] - DuracionSonido) && (numaleatorio <= segundosRandom[i] + DuracionSonido))
                {
                    posible = false;
                }

            }
            return posible;
        }
        public void OnTicked()
        {
            //aqui si el Time.TotalSeconds = al primer elemento de mi TiemposEjecutar[i] emito sonido

            if (TiemposEjecutar[IndiceTiemposEjecutar] == Time.TotalSeconds)
            {

                player.Play();

                if (IndiceTiemposEjecutar < CantidadRepeticion - 1)
                {
                    IndiceTiemposEjecutar++;
                }
            }

            Time += TimeSpan.FromSeconds(1);
            TiempoRestante = (TiempoTotal - Time);
            Minuto.Text = TiempoRestante.Minutes.ToString();
            Segundo.Text = TiempoRestante.Seconds.ToString();
            Tiemposlider.Value = Time.TotalSeconds;


        }

        public async void OnCompleted()
        {
            if (!_ispausa)
            {
                Minuto.Text = "0";
                Segundo.Text = "0";
                Time = TimeSpan.Zero;
                TiempoTotal = new TimeSpan(0, 0, Duracion);
                TiempoRestante = TimeSpan.Zero;
                TiempoRestante = TimeSpan.Zero;
                Tiemposlider.Value = 0;
                //ValorFinPLay.Text = Resultados.Count().ToString() + "  " + string.Join(", ", Resultados);
                await Application.Current.MainPage.DisplayAlert("Resultado",
                "En " + (aciertos + desaciertos).ToString() + " intentos has acertado: " + aciertos.ToString(),
                "Aceptar");

                Resultados.Clear();
                aciertos = 0;
                desaciertos = 0;
            }
        }
    }
}