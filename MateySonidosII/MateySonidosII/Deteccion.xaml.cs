﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Plugin.SimpleAudioPlayer;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MateySonidosII
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class Deteccion : ContentPage
    {
        public ControlArrastre PermisoArrastrar;

        public event Action Completed;
        public event Action Ticked;
        public Boolean ticked;
        public Boolean _isrunning;
        public Boolean _ispausa;
        public TimeSpan Time;
        public TimeSpan TiempoTotal;
        public TimeSpan TiempoRestante;
        public int Duracion; // duracion del ejercicio en segundos
        public int[] TiemposEjecutar;
        public int[] OrdenSonido;
        public int IndiceOrdenSonido;
        public List<int> Resultados;
        public int IndiceTiemposEjecutar;

        //public ISimpleAudioPlayer sonido1;
        //public ISimpleAudioPlayer sonido2;
        //public ISimpleAudioPlayer sonido3;
        public ISimpleAudioPlayer player;

        public Boolean _unicavez;
        public int aciertos;
        public int desaciertos;
        public int CantidadRepeticion = 3; //cantidad de veces que quiero se repetia el sonido por ejercicio

        public int DuracionSonido = 2;

        public Deteccion()
        {
            InitializeComponent();

            PermisoArrastrar = new ControlArrastre();
            BindingContext = PermisoArrastrar;

            InitControls();
        }

        private void InitControls()
        {
            btnPlay.Clicked += BtnPlayClicked;
            btnPausa.Clicked += BtnPausaClicked;
            btnStop.Clicked += BtnStopClicked;

            _isrunning = true;
            _ispausa = false;
            Ticked += OnTicked;
            Completed += OnCompleted;

            _unicavez = true;
            Resultados = new List<int>();
            aciertos = 0;
            desaciertos = 0;

            Duracion = 30;
            Time = TimeSpan.Zero;
            TiempoTotal = new TimeSpan(0, 0, Duracion);
            Tiemposlider.Maximum = Duracion;
            TiempoRestante = new TimeSpan();
            TiempoRestante = TimeSpan.Zero;
        }

        private void EndPlay(object sender, EventArgs e)
        {
            if (_unicavez)
            {
                Resultados.Add(0);
                desaciertos++;
            }
            else
            {
                _unicavez = true;
            }

        }

        private void BtnStopClicked(object sender, EventArgs e)
        {
            PermisoArrastrar.PermitirArrastrar = false;
            PermisoArrastre.AllowDrop = false;

            _isrunning = false;
            btnPlay.IsEnabled = true;
            Completed?.Invoke();

        }

        private void BtnPausaClicked(object sender, EventArgs e)
        {
            PermisoArrastrar.PermitirArrastrar = false;
            PermisoArrastre.AllowDrop = false;

            _isrunning = false;
            _ispausa = true;
            //btnPlay.IsEnabled = true;

        }
        private void BtnPlayClicked(object sender, EventArgs e)
        {
            PermisoArrastrar.PermitirArrastrar = true;
            PermisoArrastre.AllowDrop = true;
            //Prueba.Text = PermisoArrastre.AllowDrop.ToString();

            //si vuelvo de una pausa no ejeuto
            if (!_ispausa)
            {
                TiemposEjecutar = GenerarTiemposEjecucion();
                Array.Sort(TiemposEjecutar);
                IndiceTiemposEjecutar = 0;

                OrdenSonido = GenerarOrdenSonido();

                player = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
                player.PlaybackEnded += EndPlay;

                //aqui cargo cada sonido para luego darle play
                //var stream1 = GetStreamFromFile("tambor.mp3"); //OrdenSonido[0]
                //sonido1 = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
                //var stream2 = GetStreamFromFile("pajaros.mp3");
                //sonido2 = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
                //var stream3 = GetStreamFromFile("trompeta.mp3");
                //sonido3 = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();

                //sonido1.Load(stream1);
                //sonido2.Load(stream2);
                //sonido3.Load(stream3);

                IndiceOrdenSonido = 0;
            }

            //btnPlay.IsEnabled = false;
            _isrunning = true;
            _ispausa = false;


            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                ticked = (Time.TotalSeconds < TiempoTotal.TotalSeconds) && _isrunning;
                if (ticked)
                {
                    Ticked?.Invoke();
                }
                else
                {
                    Completed?.Invoke();
                }
                return ticked;
            });

        }

        public int[] GenerarOrdenSonido()   // Aqui genero un orden aleatorio en el que se emitiran los sonidos: Tambor, trompeta o carcajada
        {
            var ordenRandom = new int[CantidadRepeticion];
            var random = new Random();
            var paraOrdenar = new int[CantidadRepeticion];

            //guardo el total del veces - Cantidad de Repetciciones - en un array para el shuffle
            for (int i = 0; i < CantidadRepeticion; i++)
            {
                paraOrdenar[i] = i;
            }

            // primera vez
            var orden = random.Next(0, CantidadRepeticion - 1);
            ordenRandom[0] = paraOrdenar[orden];
            int temp = paraOrdenar[orden];
            paraOrdenar[orden] = paraOrdenar[CantidadRepeticion - 1];
            paraOrdenar[CantidadRepeticion - 1] = temp;

            var numrestantes = CantidadRepeticion - 1;

            var indiceordenRandom = 1;
            for (int i = 1; i < numrestantes;)
            {
                orden = random.Next(0, numrestantes - 1);
                ordenRandom[indiceordenRandom] = paraOrdenar[orden];
                temp = paraOrdenar[orden];
                paraOrdenar[orden] = paraOrdenar[numrestantes - 1];
                paraOrdenar[numrestantes - 1] = temp;
                numrestantes--;
                indiceordenRandom++;
            }
            //ultima vez
            ordenRandom[CantidadRepeticion - 1] = paraOrdenar[0];

           // OSonidos.Text = string.Join(", ", ordenRandom);
            return ordenRandom;

        }


        private int[] GenerarTiemposEjecucion()  //Aqui genero aleatoriamente en que tiempos (segundos) se emitiran los sonidos
        {
            var random = new Random();
            //cantidad de numero aleatorios que quiero
            var SegundosRandom = new int[CantidadRepeticion];

            // la primera vez
            int numaleatorio = random.Next(1, Duracion - DuracionSonido);
            SegundosRandom[0] = numaleatorio;

            for (int i = 1; i < CantidadRepeticion;)
            {
                numaleatorio = random.Next(1, Duracion - DuracionSonido);
                // veo si esta permitido
                if (Permitido(numaleatorio, SegundosRandom))
                {
                    SegundosRandom[i] = numaleatorio;
                    i++;
                }
            }

            Array.Sort(SegundosRandom);
            //PruebaSegundos.Text = string.Join(", ", SegundosRandom);
            return SegundosRandom;
        }

        private bool Permitido(int numaleatorio, int[] segundosRandom)
        {
            bool posible = true;
            for (int i = 0; i < CantidadRepeticion && posible; i++)
            {
                if ((numaleatorio >= segundosRandom[i] - DuracionSonido) && (numaleatorio <= segundosRandom[i] + DuracionSonido))
                {
                    posible = false;
                }

            }
            return posible;
        }

        public void OnTicked()
        {
            if (FrameImagen.BackgroundColor != Color.Default)
            {
                FrameImagen.BackgroundColor = Color.Default;
            }


            //aqui si el Time.TotalSeconds = al primer elemento de mi TiemposEjecutar[i] emito sonido

            if (TiemposEjecutar[IndiceTiemposEjecutar] == Time.TotalSeconds)
            {
                //PermisoArrastrar.PermitirArrastrar = true;
                //PermisoArrastre.AllowDrop = true;

                if (OrdenSonido[IndiceOrdenSonido] == 0)
                {
                    var stream = GetStreamFromFile("tambor.mp3");
                    //player = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
                    player.Load(stream);
                    player.Play();

                    //sonido1.Play();
                }
                else
                {
                    if (OrdenSonido[IndiceOrdenSonido] == 1)
                    {
                        var stream = GetStreamFromFile("pajaros.mp3");
                        //player = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
                        player.Load(stream);
                        player.Play();

                        //sonido2.Play();
                    }
                    else
                    {
                        var stream = GetStreamFromFile("trompeta.mp3");
                        //player = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
                        player.Load(stream);
                        player.Play();

                        //sonido3.Play();
                    }
                }
                IndiceOrdenSonido++;

                if (IndiceTiemposEjecutar < CantidadRepeticion - 1)
                {
                    IndiceTiemposEjecutar++;
                }
               // player.PlaybackEnded += EndPlay;
            }

            Time += TimeSpan.FromSeconds(1);
            TiempoRestante = (TiempoTotal - Time);
            Minuto.Text = TiempoRestante.Minutes.ToString();
            Segundo.Text = TiempoRestante.Seconds.ToString();
            Tiemposlider.Value = Time.TotalSeconds;

        }
        Stream GetStreamFromFile(string filename)
        {
            var assembly = typeof(App).GetTypeInfo().Assembly;

            var stream = assembly.GetManifestResourceStream("MateySonidosII." + filename);

            return stream;
        }

        public async void OnCompleted()
        {
            if (!_ispausa)
            {
                Minuto.Text = "0";
                Segundo.Text = "0";
                Time = TimeSpan.Zero;
                TiempoTotal = new TimeSpan(0, 0, Duracion);
                TiempoRestante = TimeSpan.Zero;
                TiempoRestante = TimeSpan.Zero;
                Tiemposlider.Value = 0;
                //ValorFinPLay.Text = Resultados.Count().ToString() + "  " + string.Join(", ", Resultados);
                await Application.Current.MainPage.DisplayAlert("Resultado",
                "En " + (aciertos + desaciertos).ToString() + " intentos has acertado: " + aciertos.ToString(),
                "Aceptar");

                Resultados.Clear();
                aciertos = 0;
                desaciertos = 0;
                PermisoArrastrar.PermitirArrastrar = false;
                PermisoArrastre.AllowDrop = false;
            }
        }

        void OnDrop(object sender, DropEventArgs e)
        {
            if (player.IsPlaying)
            {
               // OSonidos.Text = "Evento Ondragover - sin sonido - ARRASTRE CORRECTO";
                if (_unicavez)
                {
                    _unicavez = false;
                    Resultados.Add(1);
                    aciertos++;
                }
                FrameImagen.BackgroundColor = Color.Green;
            }
            else
            {
                Resultados.Add(0);
                desaciertos++;
                FrameImagen.BackgroundColor = Color.Red;
                //OSonidos.Text = "Evento Ondragover - sin sonido - ARRASTRE INCORRECTO";
            }
        }

    }
}