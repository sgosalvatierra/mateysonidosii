﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using MateySonidosII.Utils;


namespace MateySonidosII
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            lblTitulo.VerticalTextAlignment = TextAlignment.Center;
            lblTitulo.HorizontalTextAlignment = TextAlignment.Center;
        }
        //*************************************************************************************************************************************
        //*************************************************************************************************************************************
        //METODOS
        //*************************************************************************************************************************************
        //*************************************************************************************************************************************
        private void ClickBtnIniciar(object sender, EventArgs e)
        {
            //lblTitulo.Text = "AlgeRA";

            //this.Navigation.PushModalAsync(new VisorRAPage());

            DependencyService.Get<ILaunchActivityOrViewController>().Launch("");

            ////Android
            //if (Device.RuntimePlatform == Device.Android)
            //{


            //    //iOS
            //}else if (Device.RuntimePlatform == Device.iOS)
            //{

            //}
        }
    }
}
