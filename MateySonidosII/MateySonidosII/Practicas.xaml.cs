﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MateySonidosII.Modelos;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MateySonidosII
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Practicas : ContentPage
    {
        private static bool banClick;
        public Practicas()
        {
            InitializeComponent();
            Title = "Ejercicios de (re) Habilitacion";

            banClick = true;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            ListaOpcionEjercicio opciones = new ListaOpcionEjercicio();

            lvLista.ItemsSource = opciones._opcionesejercicios;
            lvLista.ItemSelected += LvLista_ItemSelected;

        }

        private async void LvLista_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (banClick)
            {
                var item = e.SelectedItem as ListaEjercicios;

                if (item != null)
                {
                    banClick = false;

                    switch (item.OpcionEjercicio)
                    {
                        case "Deteccion":
                            {
                                //Carga nueva pagina 
                                //await Application.Current.MainPage.Navigation.PushAsync(new SonidosLingPage());
                                await Navigation.PushAsync(new Deteccion());
                                //IsPresented = false;
                            }
                            break;

                        case "Discriminacion":

                            //await Application.Current.MainPage.Navigation.PushAsync(new SonidosLingIndivPage());
                            await Navigation.PushAsync(new Discriminacion());

                            break;

                        case "Identificacion":

                            //await Application.Current.MainPage.Navigation.PushAsync(new SonidosLingIndivPage());
                            await Navigation.PushAsync(new Identificacion());

                            break;

                        case "Comprension":

                            //await Application.Current.MainPage.Navigation.PushAsync(new SonidosLingIndivPage());
                            await Navigation.PushAsync(new Comprension());

                            break;
                    }

                    await Task.Run(async () =>
                    {
                        await Task.Delay(500);
                        banClick = true;
                    });

                }
            }
        }
    }
}