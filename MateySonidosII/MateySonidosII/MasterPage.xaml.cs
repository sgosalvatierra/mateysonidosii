﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MateySonidosII
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
 
    public partial class MasterPage : MasterDetailPage
    {
        private List<MenuItems> menu;
        public MasterPage()
        {
            InitializeComponent();
            //Menu
            menu = new List<MenuItems>();
            //Opciones de menu
            menu.Add(new MenuItems { OptionName = "Inicio" });
            menu.Add(new MenuItems { OptionName = "Sonidos de Ling" });
            menu.Add(new MenuItems { OptionName = "Ejercicios de (re) Habilitacion auditiva" });
            menu.Add(new MenuItems { OptionName = "Configuración" });
            menu.Add(new MenuItems { OptionName = "Ayuda" });
            menu.Add(new MenuItems { OptionName = "Ingresar" });

            //NavList, se asigna menu
            navigationList.ItemsSource = menu;

            //Primera pagina a mostrar
            Detail = new NavigationPage(new MainPage());
        }
        private void Item_Tapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var item = e.Item as MenuItems;

                switch (item.OptionName)
                {
                    case "Inicio":
                        {
                            //Carga nueva pagina 
                            //Detail = new NavigationPage(new MainPage());
                            IsPresented = false;
                        }
                        break;

                    case "Sonidos de Ling":
                        {
                            //Carga nueva pagina en el navigation stack
                            Detail.Navigation.PushAsync(new SonidosLing());
                            IsPresented = false;
                        }
                        break;

                    case "Ejercicios de (re) Habilitacion auditiva":
                        {
                            //Carga nueva pagina en el navigation stack
                            Detail.Navigation.PushAsync(new Practicas());
                            IsPresented = false;
                        }
                        break;

                    case "Configuración":
                        {
                            //Carga nueva pagina en el navigation stack
                            // Detail.Navigation.PushAsync(new Practica());
                            IsPresented = false;
                        }
                        break;

                    case "":
                        {
                            //Carga nueva pagina en el navigation stack
                            // Detail.Navigation.PushAsync(new Practica());
                            IsPresented = false;
                        }
                        break;

                    case "Ayuda":
                        {
                            //Carga nueva pagina en el navigation stack
                            // Detail.Navigation.PushAsync(new Practica());
                            IsPresented = false;
                        }
                        break;

                    case "Ingresar":
                        {
                            //Carga nueva pagina en el navigation stack
                            // Detail.Navigation.PushAsync(new Ingresar());
                            IsPresented = false;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
   
    public class MenuItems
    {
        public string OptionName { get; set; }
    }
}