﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MateySonidosII.Modelos
{
    public class ListaEjercicios
    {
        public string OpcionEjercicio { get; set; }
        public int id { get; set; }
    }

    public class ListaOpcionEjercicio
    {
        public List<ListaEjercicios> _opcionesejercicios { get; set; }

        public ListaOpcionEjercicio()
        {
            _opcionesejercicios = new List<ListaEjercicios>();
            CargarLista();
        }

        private void CargarLista()
        {
            _opcionesejercicios.Add(new ListaEjercicios
            {
                OpcionEjercicio = "Deteccion",
                id = 1
            });
            _opcionesejercicios.Add(new ListaEjercicios
            {
                OpcionEjercicio = "Discriminacion",
                id = 2
            });
            _opcionesejercicios.Add(new ListaEjercicios
            {
                OpcionEjercicio = "Identificacion",
                id = 3
            });
            _opcionesejercicios.Add(new ListaEjercicios
            {
                OpcionEjercicio = "Comprension",
                id = 4
            });
        }
    }
}

