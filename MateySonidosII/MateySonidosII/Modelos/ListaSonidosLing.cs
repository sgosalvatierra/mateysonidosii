﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MateySonidosII.Modelos
{
    public class ListaSonidosLing
    {
        public string Opcion { get; set; }
        public int id { get; set; }
    }

    public class ListaOpciones
    {
        public List<ListaSonidosLing> _listaopciones { get; set; }

        public ListaOpciones()
        {
            _listaopciones = new List<ListaSonidosLing>();
            CargaLista();
        }

        public void CargaLista()
        {
            _listaopciones.Add(new ListaSonidosLing
            {
                Opcion = "Descripción del Test propuesto",
                id = 1
            });
            _listaopciones.Add(new ListaSonidosLing
            {
                Opcion = "Test de Ling",
                id = 2
            });
        }
    }
}
